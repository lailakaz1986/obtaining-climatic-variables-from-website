#!/bin/bash 
#Autora: Laila Daniela Kazimierski. Agosto 2020.
#Primero hay que hacer este archivo .sh ejecutable. Para eso, en terminal: chmod +x nombredelarchivo.sh
archivo=$1 #El arhivo con el que estoy trabajando es cauquenes_estaciones_smn. En terminal: ./extrae_datos_clima.sh cauquenes_estaciones_smn

cat $archivo | awk -F "\t" 'NR>1 {printf "%s;%.2f;%i\n",$6,$7,$23}' > cauquenes_estaciones.txt #Me quedo con las columnas fecha (en formato dia/mes/año), hora, ID de la estación SMN sin la primera fila.

#for line in `head -n2 cauquenes_estaciones.txt`;  do #Para hacer pruebas, uso solo dos líneas del archivo
IFS=$'\n'
for line in `cat cauquenes_estaciones.txt`;  do #Lee línea por línea del archivo
	fecha=`echo $line | cut -d ";" -f1` #La fecha corresponde al dato de la primera "columna" (separado por ";" del archivo "cauquenes_estaciones.txt")
	dia=`echo $fecha | cut -d "/" -f1` #Tomando como separador "/", el primer dato de "fecha" es el dia
	mes=`echo $fecha | cut -d "/" -f2` #Tomando como separador "/", el segundo dato de "fecha" es el mes
	ano=`echo $fecha | cut -d "/" -f3` #Tomando como separador "/", el tercer dato de "fecha" es el año
	horario=`echo $line | cut -d ";" -f2` #El horario corresponde al dato de la segunda "columna" (separado por ";") del archivo "cauquenes_estaciones.txt"
	estacion=`echo $line | cut -d ";" -f3` #La estación del SMN corresponde al dato de la tercera "columna" (separado por ";") del archivo "cauquenes_estaciones.txt"
#echo $ano $mes $dia $hora $estacion #Muestra en pantalla los datos extraídos
	hora=`echo $horario | cut -d "." -f1` #La hora corresponde al dato de la variable $horario sin los minutos (primera "columna" (separado por ".")) 
	minutos=`echo $horario | cut -d "." -f2` #Los minutos corresponden al segundo dato de la variable $horario (separado por ".")
	linea=$line
#echo $ano $mes $dia $hora $estacion #Muestra en pantalla los datos extraídos
	wget --no-check-certificate "https://www.ogimet.com/cgi-bin/gsynres?ind=$estacion&ndays=1&ano=$ano&mes=$mes&day=$dia&hora=$hora&ord=REV&enviar=Ver" -O web.txt

	#Genero el archivo de variables climáticas
	cat web.txt | grep -e "<TH " | grep -e "</TH>$" | sed 's/<[^>]*>//g' | grep -v "Fecha" | grep -v "Diariometeorol&#243" > 	variables.txt #grep -v se queda con el complemento (grep -v "Fecha" se queda con todos los campos excepto con "Fecha")

	#Genero el archivo con valores de las variables climáticas
	cat web.txt | grep -e "^<TD align" | grep -e "</TD>$" | sed 's/<[^>]*>//g' > valores.txt #Genero un archivo aparte que tenga solamente los valores de las variables (filtro cosas que empiezan con <TD align y terminan con </TD>. Con "sed" reemplazo todo lo que está entre <> por nada.

	#Guardo en "variables" las variables climáticas
	for line in `cat variables.txt`;  do #Leo cada linea del archivo de variables
		variables="$variables;$line"
	done

	#Guardo en "variables" las variables climáticas
	for line in `cat valores.txt`;  do #Leo cada linea del archivo de valores de variables climáticas
		valores="$valores;$line"
	done

	#Agrego en cada línea del archivo cauquenes_estaciones.txt las columnas con las variables climáticas y los valores de esas variables
	echo -e "$linea$variables$valores" >> cauquenes_estaciones_nuevo.txt 

	#Hago un clear de las variables
	variables=""
	valores=""
	linea=""
done
